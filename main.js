const sendButton = document.querySelector('.send')
const editButton = document.querySelector('.edit')
const deleteButton = document.querySelector('.delete')
let text = document.querySelector('#write')

sendButton.addEventListener('click', () => {
  if (text.value != '') {
    var newMessage = document.createElement('div')
    newMessage.classList.add('message-flow')
    var messagePanel = document.querySelector('.message-panel')
    newMessage.innerHTML = `<div class="message">
    <textarea
      name="new-message"
      class="message-sent"
      cols="70"
      rows="2"
      style="width: 48.56rem; height: 3.875rem"
      readonly
    ></textarea>
    <div class="buttons">
      <input class="edit" type="button" value="Editar" onclick="editMessage(this)"/>
      <input class="delete" type="button" value="Excluir" onclick="deleteMessage(this)" />
    </div>
  </div>`
    newMessage.firstElementChild.firstElementChild.value += text.value
    messagePanel.appendChild(newMessage)
    text.value = ''
    return
  } else {
    alert('Por favor insira uma mensagem')
  }
})

function deleteMessage(el) {
  element = el
  let div = element.parentNode.parentNode
  div.remove()
  return
}

function editMessage(el) {
  element = el
  let div = element.parentNode.parentNode
  textbox = div.firstElementChild
  textbox.toggleAttribute('readonly')
  return
}
